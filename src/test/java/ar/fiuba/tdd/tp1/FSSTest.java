package ar.fiuba.tdd.tp1;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

import java.util.LinkedList;

public class FSSTest {

    @Test
    public void setAndGetCellValue() {
        Book book = new Book(new LinkedList<OperationParser>());
        Sheet sheet = book.createANewSheet("hoja1", 10, 10);
        Cell cell1 = sheet.getCell(5, 5);
        cell1.setValue("a");
        Cell cell2 = sheet.getCell(5, 5);
        assertTrue(cell2.getValue().equals("a"));
    }
    
    @Test
    public void doubleSetValueAndUndo() {
        Book book = new Book(new LinkedList<OperationParser>());
        Sheet sheet = book.createANewSheet("hoja1", 10, 10);
        Cell cell1 = sheet.getCell(5, 5);
        cell1.setValue("a");
        cell1.setValue("b");
        book.undo();
        Cell cell2 = sheet.getCell(5, 5);
        assertTrue(cell2.getValue().equals("a"));
    }   
    
    @Test
    public void doubleSetValueAndUndoRedo() {
        Book book = new Book(new LinkedList<OperationParser>());
        Sheet sheet = book.createANewSheet("hoja1", 10, 10);
        Cell cell1 = sheet.getCell(5, 5);
        cell1.setValue("a");
        cell1.setValue("b");
        book.undo();
        book.redo();
        Cell cell2 = sheet.getCell(5, 5);
        assertTrue(cell2.getValue().equals("b"));
    }  
    
    @Test
    public void resolveFormulaAddAndSub() {
    	LinkedList<OperationParser> parsers = new LinkedList<OperationParser>();
    	parsers.add(new AdditionParser());
    	parsers.add(new SubstractionParser());
        Book book = new Book(parsers);
        Sheet sheet = book.createANewSheet("hoja1", 10, 10);
        
        //F6
        Cell cell1 = sheet.getCell(5, 5); 
        cell1.setValue("10");
        
        //C3
        Cell cell2 = sheet.getCell(2,2);
        cell2.setValue("15");
        
        //formula = 10+15-6+10 = 29
        Cell cell3 = sheet.getCell(1,1);
        cell3.setFormula("F6+C3-6+F6");
        assertTrue(cell3.getValue().equals("29.0"));
    }
    
    @Test
    public void resolveFormulaAddUndo() {
    	LinkedList<OperationParser> parsers = new LinkedList<OperationParser>();
    	parsers.add(new AdditionParser());
    	parsers.add(new SubstractionParser());
        Book book = new Book(parsers);
        Sheet sheet = book.createANewSheet("hoja1", 10, 10);
        
        //F6
        Cell cell1 = sheet.getCell(5, 5); 
        cell1.setValue("10");
        
        //C3
        Cell cell2 = sheet.getCell(2,2);
        cell2.setValue("15");
        
        //formula = 10+15-6+10 = 29
        Cell cell3 = sheet.getCell(1,1);
        cell3.setFormula("F6+C3-6+F6");
        book.undo();
        assertTrue(cell3.getValue() == null);
    }
    
    @Test
    public void resolveFormulaAddUndoRedo() {
    	LinkedList<OperationParser> parsers = new LinkedList<OperationParser>();
    	parsers.add(new AdditionParser());
    	parsers.add(new SubstractionParser());
        Book book = new Book(parsers);
        Sheet sheet = book.createANewSheet("hoja1", 10, 10);
        
        //F6
        Cell cell1 = sheet.getCell(5, 5); 
        cell1.setValue("10");
        
        //C3
        Cell cell2 = sheet.getCell(2,2);
        cell2.setValue("15");
        
        //formula B2: 10+15-6+10 = 29
        Cell cell3 = sheet.getCell(1,1);
        cell3.setFormula("F6+C3-6+F6");
        book.undo();
        book.redo();
        assertTrue(cell3.getValue().equals("29.0"));
    }
    
    @Test
    public void overloadFormulas() {
    	LinkedList<OperationParser> parsers = new LinkedList<OperationParser>();
    	parsers.add(new AdditionParser());
    	parsers.add(new SubstractionParser());
        Book book = new Book(parsers);
        Sheet sheet = book.createANewSheet("hoja1", 10, 10);
        
        //F6
        Cell cell1 = sheet.getCell(5, 5); 
        cell1.setValue("10");
        
        //C3
        Cell cell2 = sheet.getCell(2,2);
        cell2.setValue("15");
        
        //formula B2: 10+15-6+10 = 29 
        Cell cell3 = sheet.getCell(1,1);
        cell3.setFormula("F6+C3-6+F6");
        
        Cell cell4 = sheet.getCell(1,2);
        cell4.setFormula("B2+1");
        
        assertTrue(cell4.getValue().equals("30.0"));
    }   
    
    @Test
    public void cloneCellValue() {
    	LinkedList<OperationParser> parsers = new LinkedList<OperationParser>();
    	parsers.add(new AdditionParser());
    	parsers.add(new SubstractionParser());
        Book book = new Book(parsers);
        Sheet sheet = book.createANewSheet("hoja1", 10, 10);
        
        //F6
        Cell cell1 = sheet.getCell(5, 5); 
        cell1.setValue("10");
        
        Cell cell2 = sheet.getCell(1,2);
        cell2.setFormula("F6");
        
        book.undo();
        book.redo();
        assertTrue(cell2.getValue().equals("10.0"));
    }  
    
    @Test
    public void sumCellWithoutValue() {
    	LinkedList<OperationParser> parsers = new LinkedList<OperationParser>();
    	parsers.add(new AdditionParser());
    	parsers.add(new SubstractionParser());
        Book book = new Book(parsers);
        Sheet sheet = book.createANewSheet("hoja1", 10, 10);
        
        //F6
        Cell cell1 = sheet.getCell(5, 5); 
        cell1.setValue("10");
        
        Cell cell2 = sheet.getCell(5,5);
        cell2.setFormula("F6+A1");
        
        assertTrue(cell2.getValue().equals("10"));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void setValueNullToACell() {
        Book book = new Book(new LinkedList<OperationParser>());
        Sheet sheet = book.createANewSheet("hoja1", 10, 10);
        Cell cell = sheet.getCell(5, 5);
        cell.setValue(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void setValueEmptyToACell() {
        Book book = new Book(new LinkedList<OperationParser>());
        Sheet sheet = book.createANewSheet("hoja1", 10, 10);
        Cell cell = sheet.getCell(5, 5);
        cell.setValue("");
    }
    
    @Test
    public void getCalculateFormulaWithCellsFromDifferentSheets() {
    	LinkedList<OperationParser> parsers = new LinkedList<OperationParser>();
    	parsers.add(new AdditionParser());
    	parsers.add(new SubstractionParser());
        Book book = new Book(parsers);
        Sheet sheet1 = book.createANewSheet("hoja1", 10, 10);
        Sheet sheet2 = book.createANewSheet("hoja2", 10, 10);
        
        //F6 (sheet 1)
        Cell cell1 = sheet1.getCell(5, 5); 
        cell1.setValue("10");
        
        //C3 (sheet 1)
        Cell cell2 = sheet1.getCell(2,2);
        cell2.setValue("15");
        
        //A1 (sheet 2)
        Cell cell3 = sheet2.getCell(0,0);
        cell3.setValue("15");
        
        //formula = 10+15-6+10 = 29
        Cell cell4 = sheet1.getCell(1,1);
        cell4.setFormula("F6+C3-6+F6+hoja2!A1");
        assertTrue(cell4.getValue().equals("44.0"));
    }
    
    @Test
    public void invalidFormula() {
    	LinkedList<OperationParser> parsers = new LinkedList<OperationParser>();
    	parsers.add(new AdditionParser());
    	parsers.add(new SubstractionParser());
        Book book = new Book(parsers);
        Sheet sheet1 = book.createANewSheet("hoja1", 10, 10);
      
        Cell cell1 = sheet1.getCell(1,1);
        cell1.setFormula("F6>invalido");
        assertTrue(cell1.getValue().equals("Formula invalida"));
    }
}