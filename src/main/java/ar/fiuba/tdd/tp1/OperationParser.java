package ar.fiuba.tdd.tp1;

import java.util.LinkedList;

public abstract class OperationParser {

    public abstract AOperation parse(Cell targetCell, String formula, int nextSymbolPosition);
    
    public abstract String getSymbol();

    public int firstSymbolMatchPosition(String formula) {
        return formula.indexOf(getSymbol());
    }

    public LinkedList<Integer> symbolMatchPositions(String formula) {
        LinkedList<Integer> positions = new LinkedList<Integer>();
        int indexStartSearching = 0;
        while (indexStartSearching < formula.length()) {
            int index = formula.indexOf(getSymbol(),indexStartSearching);
            if (index != -1) {
                positions.add(index);
                indexStartSearching = index + 1;
            } else {
                return positions;
            }
        }
        return positions;
    }

}