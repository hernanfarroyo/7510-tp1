package ar.fiuba.tdd.tp1;

public class SubstractionParser extends BinaryOperationParser {

    @Override
    public AOperation parse(Cell targetCell, String formula, int nextSymbolPosition) {
        return new Subtraction(targetCell, getLeft(formula), getRight(formula, nextSymbolPosition));
    }

    @Override
    public String getSymbol() {
        return "-";
    }

}