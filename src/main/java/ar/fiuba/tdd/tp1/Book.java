package ar.fiuba.tdd.tp1;

import java.util.LinkedList;

public class Book {

    private LinkedList<Sheet> sheets;
    private OperationManager operationManager;
    LinkedList<OperationParser> parsers;

    public Book(LinkedList<OperationParser> parsers) {
        sheets = new LinkedList<Sheet>();
        operationManager = new OperationManager();
        this.parsers = parsers;
    }

    public Sheet createANewSheet(String name, int numberOfRows, int numberOfColumns) {
        Sheet newSheet = new Sheet(this, name, numberOfRows, numberOfColumns);
        sheets.add(newSheet);
        return newSheet;
    }

    public void deleteASheet(Sheet sheet) {
        sheets.remove(sheet);
    }
    
    public LinkedList<OperationParser> getOperationParsers() {
        return parsers;
    }
    
    public void addOperation(AOperation operation) {
        operationManager.addNewOperation(operation);
    }
    
    public void undo() {
        this.operationManager.undo();
    }
    
    public void redo() {
        this.operationManager.redo();
    }
    
    public Sheet getSheet(String sheetName) {
        for (Sheet sheet : sheets ) {
            if (sheet.getName().equals(sheetName)) {
                return sheet;
            }
        }
        throw new IllegalArgumentException();
    }
    
    public Cell getCell(String identifier) {
        String[] sheetNameAndCellId = identifier.split("!");
        if (sheetNameAndCellId.length > 2) {
            throw new IllegalArgumentException();
        }
        Sheet sheet = getSheet(sheetNameAndCellId[0]);
        return sheet.getCell(sheetNameAndCellId[1]);
    }
    
}