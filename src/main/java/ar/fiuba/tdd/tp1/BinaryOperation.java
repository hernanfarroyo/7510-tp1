package ar.fiuba.tdd.tp1;

public abstract class BinaryOperation extends AOperation {

    private String left;
    private String right;
    
    public abstract Double calculateResult();

    public BinaryOperation(Cell targetCell, String left, String right) {
        super.setTargetCell(targetCell);
        this.left = left;
        this.right = right;
    }

    public String getLeft() {
        return left;
    }
    
    public String getRight() {
        return right;
    }
    
    public void execute() {
        String value = this.getTargetCell().getValue();
        super.setOldValue(value);
        Double result = calculateResult();
        super.setNewValue(result.toString());
        this.getTargetCell().writeValue(result.toString());
    }

}