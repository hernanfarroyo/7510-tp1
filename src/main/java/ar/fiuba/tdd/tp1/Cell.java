package ar.fiuba.tdd.tp1;

public class Cell {

    private String identifier;
    private String formula;
    private String value;
    private Sheet sheet;

    public Cell(Sheet sheet, String identifier) {
        this.sheet = sheet;
        this.identifier = identifier;
        this.value = null;
    }

    public Cell(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setFormula(String formula) {
        this.formula = formula.replaceAll(" ", "");
        calculate();
    }

    public void setValue(String value) {
        if ((value == null) || (value.isEmpty())) {
            throw new IllegalArgumentException();
        }
        AOperation operation = new Write(this, value);
        this.sheet.getBook().addOperation(operation);
        operation.execute();
    }
    
    public void writeValue(String value) {
        this.value = value;
    }

    public String getFormula() {
        return formula;
    }

    public String getValue() {
        return value;
    }

    public Double getOperand(String operand) {
        Double number;
        try {
            number =  Double.parseDouble(operand);
        } catch (NumberFormatException e) {
            number = Double.parseDouble(sheet.getCell(operand).getValue());
        }
        return number;
    }
    
    public void calculate() {
        if (!formula.isEmpty()) {
            try {
                value = Double.toString(getOperand(formula));
            } catch (IllegalArgumentException e) {
                AOperation operation = new Operation(this);
                this.sheet.getBook().addOperation(operation);
                try {
                    operation.execute();
                } catch (NullPointerException e2) {
                    operation.unexecute();
                }
            }
        }
    }
    
    public Sheet getSheet() {
        return sheet;
    }

}