package ar.fiuba.tdd.tp1;

public abstract class AOperation {

    private String oldValue;
    private String newValue;
    private Cell targetCell;

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }
    
    public void setTargetCell(Cell cell) {
        this.targetCell = cell;
    }
    
    public Cell getTargetCell() {
        return this.targetCell;
    }
    
    public Double getOperand(String operand) {
        try {
            return Double.parseDouble(operand);
        } catch (NumberFormatException e) {
            return Double.parseDouble(targetCell.getSheet().getCell(operand).getValue());
        }
    }
    
    public void unexecute() {
        this.targetCell.writeValue(oldValue);
    }
    
    abstract void execute();
    
}