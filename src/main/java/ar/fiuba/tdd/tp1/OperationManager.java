package ar.fiuba.tdd.tp1;

import java.util.Stack;

public class OperationManager {

    private Stack<AOperation> undoOperations;
    private Stack<AOperation> redoOperations;

    public OperationManager() {
        undoOperations = new Stack<AOperation>();
        redoOperations = new Stack<AOperation>();
    }

    public void addNewOperation(AOperation operation) {
        undoOperations.push(operation);
    }

    public void undo() {
        AOperation operationToUndo = undoOperations.pop();
        redoOperations.push(operationToUndo);
        operationToUndo.unexecute();
    }

    public void redo() {
        AOperation operationToRedo = redoOperations.pop();
        undoOperations.push(operationToRedo);
        operationToRedo.execute();
    }

}