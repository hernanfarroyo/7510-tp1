package ar.fiuba.tdd.tp1;

public abstract class BinaryOperationParser extends OperationParser {

    public String getLeft(String formula) {
        return formula.substring(0, firstSymbolMatchPosition(formula));
    }

    public String getRight(String formula, int nextSymbolPosition) {
        return formula.substring(firstSymbolMatchPosition(formula) + 1, nextSymbolPosition);
    }

}