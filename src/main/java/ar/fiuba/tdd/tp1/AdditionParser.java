package ar.fiuba.tdd.tp1;

public class AdditionParser extends BinaryOperationParser {

    @Override
    public AOperation parse(Cell targetCell, String formula, int nextSymbolPosition) {
        return new Addition(targetCell, getLeft(formula), getRight(formula, nextSymbolPosition));
    }

    @Override
    public int firstSymbolMatchPosition(String formula) {
        return formula.indexOf("+");
    }

    @Override
    public String getSymbol() {
        return "+";
    }

}