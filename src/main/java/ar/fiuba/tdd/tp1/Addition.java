package ar.fiuba.tdd.tp1;

public class Addition extends BinaryOperation {

    public Addition(Cell targetCell, String left, String right) {
        super(targetCell, left, right);
    }

    @Override
    public Double calculateResult() {
        return (getTargetCell().getOperand(getLeft()) + getTargetCell().getOperand(getRight()));
    }

}