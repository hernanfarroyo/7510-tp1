package ar.fiuba.tdd.tp1;

public class Sheet {

    private Book book;
    String name;
    private Cell[][] cells;
    private int rows;
    private int colums;
    
    private String convertToIdentifier(int row, int colum) {
        String identifier = columnNumberToName(colum) + (row + 1);
        return identifier;
    }
    
    public Sheet(Book book, String name, int rows, int colums) {
        this.book = book;
        this.name = name;
        cells = new Cell[rows][colums];
        
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < colums; j++) {
                String identifier = convertToIdentifier(i,j);
                cells[i][j] = new Cell(this, identifier);
            }
        }
        this.rows = rows;
        this.colums = colums;
    }
    
    public String getName() {
        return name;
    }

    public String columnNumberToName(int columNumber) {
        int auxColumNumber = columNumber + 1;
        StringBuilder sb = new StringBuilder();
        while (auxColumNumber-- > 0) {
            sb.append((char)('A' + (auxColumNumber % 26)));
            auxColumNumber /= 26;
        }
        return sb.reverse().toString();
    }

    public Cell getCell(int row, int column) {
        boolean correctRange = (row < rows) && (column < colums);
        if (correctRange) {
            return cells[row][column];
        } else {
            throw new IllegalArgumentException();
        }
    }

    public Cell getCell(String identifier) {
        if ( identifier.contains("!") ) {
            return book.getCell(identifier);
        }
        Cell cell = getCellCorrect(identifier);
        if (cell != null) {
            return cell;
        } else {
            throw new IllegalArgumentException();
        }
    }
    
    private Cell getCellCorrect(String identifier) {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < colums; j++) {
                if (cells[i][j].getIdentifier().equals(identifier)) {
                    return cells[i][j];
                }
            }
        }
        return null;
    }
    
    public Book getBook() {
        return this.book;
    }

    public void calculateFormulas() {

    }
}