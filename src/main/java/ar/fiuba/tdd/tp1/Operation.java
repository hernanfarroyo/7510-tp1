package ar.fiuba.tdd.tp1;

import java.util.Collections;
import java.util.LinkedList;

public class Operation extends AOperation {

    public Operation(Cell targetCell) {
        super.setTargetCell(targetCell);
    }

    public LinkedList<Integer> getOperatorsPositions(String formulaToParse, LinkedList<OperationParser> parsers) {
        LinkedList<Integer> operatorsPositions = new LinkedList<Integer>();
        for (OperationParser opParser : parsers) {
            LinkedList<Integer> operatorPositions = opParser.symbolMatchPositions(formulaToParse);
            operatorsPositions.addAll(operatorPositions);
        }
        Collections.sort(operatorsPositions);
        return operatorsPositions;
    }
    
    public AOperation getFirstOperator(String formulaToParse, LinkedList<OperationParser> parsers) {
        LinkedList<Integer> operatorsPositions = getOperatorsPositions(formulaToParse, parsers);

        for (OperationParser opParser : parsers) {
            if (opParser.firstSymbolMatchPosition(formulaToParse) == operatorsPositions.getFirst()) {
                if (operatorsPositions.size() > 1) {
                    return opParser.parse(getTargetCell(), formulaToParse, operatorsPositions.get(1));
                } else {
                    return opParser.parse(getTargetCell(), formulaToParse, formulaToParse.length());
                }
            }
        }
        return null;
    }
    
    public void execute() {
        setOldValue(getTargetCell().getValue());
        String formulaToParse = getTargetCell().getFormula();
        LinkedList<OperationParser> parsers = getTargetCell().getSheet().getBook().getOperationParsers();
        LinkedList<Integer> operatorsPositions = getOperatorsPositions(formulaToParse, parsers);
    
        if (operatorsPositions.size() > 0) {
            while (operatorsPositions.size() > 0) {
                AOperation operation = getFirstOperator(formulaToParse, parsers);
                operation.execute();
                formulaToParse = getFormulaToParse(operatorsPositions, parsers, formulaToParse);
                formulaToParse = operation.getNewValue() + formulaToParse;
                operatorsPositions = getOperatorsPositions(formulaToParse, parsers);
            }
            setNewValue(getTargetCell().getValue());
        } 
    }
    
    public String getFormulaToParse(LinkedList<Integer> operatorsPositions, LinkedList<OperationParser> parsers, String formulaToParse) {
        if (operatorsPositions.size() > 1) {
            formulaToParse = formulaToParse.substring(getOperatorsPositions(formulaToParse, parsers).get(1));
        } else {
            formulaToParse = "";
        }
        return formulaToParse;
    }
}