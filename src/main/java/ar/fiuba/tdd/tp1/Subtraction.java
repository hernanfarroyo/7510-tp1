package ar.fiuba.tdd.tp1;

public class Subtraction extends BinaryOperation {

    public Subtraction(Cell targetCell, String left, String right) {
        super(targetCell, left, right);
    }

    @Override
    public Double calculateResult() {
        return (getTargetCell().getOperand(getLeft()) - getTargetCell().getOperand(getRight()));
    }
}