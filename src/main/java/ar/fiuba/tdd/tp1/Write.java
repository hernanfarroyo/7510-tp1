package ar.fiuba.tdd.tp1;

public class Write extends AOperation {

    private String value;

    public Write(Cell targetCell, String value) {
        super.setTargetCell(targetCell);
        this.value = value;
    }

    public void execute() {
        super.setOldValue(this.getTargetCell().getValue());
        super.setNewValue(value);
        this.getTargetCell().writeValue(value);
    }
}